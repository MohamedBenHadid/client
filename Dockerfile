#  from les langages/framework qu'ona 
FROM node:14.17-buster
WORKDIR /App
#copier les dependences pour le porjet react ( chaque langue a sa propre dependences)
COPY package*.json /App/
#install dependences
RUN npm install
#copier les fichers sources
COPY .   /App
EXPOSE 3000
RUN npm install pm2 -g
# the same port  that we will use in file.yaml
# la commande a excuter pour lancer le projet ( pour cr�er container)
CMD  [ "npm", "run","start" ]

